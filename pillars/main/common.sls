users:
  epita:
    fullname: Epita
    home: /home/epita
    shell: /bin/bash
    empty_password: True
    groups:
      - audio
      - video

  root:
    password: $6$6Syge7o7U6d$0CUXXJtKsdrIP/By1oqDwR9wZzBBHsNtqw.k43D.Itsl2tYqpdS7AsY6uXHxkioQdJ2q2E1Z2.FUQHj7H1hAW/
    shell: /bin/bash
    home: /root
    createhome: True
    user_files:
      enabled: True
      source: salt://files/default/home/root

sudoers:
  groups:
    admins:
      - 'ALL=(ALL) ALL'

sddm-footer: "build: {{ "now"|strftime("%y%m%d-%H%m") }}"
